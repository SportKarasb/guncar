package ru.androiddevschool.guncar.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import ru.androiddevschool.guncar.Utils.Assets;
import ru.androiddevschool.guncar.View.ImageActor;
import ru.androiddevschool.guncar.View.NumberTextActor;

/**
 * Created by 03k1203 on 04.05.2017.
 */
public class Player extends ImageActor {
    public NumberTextActor liveScore;

    public Player(TextureRegion region, float x, float y, float diam) {
        super(region, x, y, diam);
        liveScore = new NumberTextActor(10, x + 10, y + 30, Assets.get().fonts.get("simple"));
    }

    public void act(float delta) {
        super.act(delta);
        liveScore.setPosition(getX() + 10, getY() + 30);
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        liveScore.draw(batch, parentAlpha);
    }

    public NumberTextActor getLives() {
        return liveScore;
    }

}