package ru.androiddevschool.guncar.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import ru.androiddevschool.guncar.GunCar;
import ru.androiddevschool.guncar.Utils.Assets;
import ru.androiddevschool.guncar.View.ImageActor;

/**
 * Created by 03k1203 on 04.05.2017.
 */
 public class World extends Stage {
    private Player player;
    private HashSet<Integer> pressedButtons;
    private ArrayList<Bullet> bullets;
    private ArrayList<Mob> mobs;
    private ArrayList<ImageActor> toDelete;
    private Vector2 stageCoords;
    private static Random r = new Random();

    public World(Viewport screenViewport, Batch batch) {
        super(screenViewport, batch);
        pressedButtons = new HashSet<Integer>();
        addActor(new Image(Assets.get().imgs.get("imgs/bg2").getRegion().getTexture()));
        pressedButtons = new HashSet<Integer>();
        bullets = new ArrayList<Bullet>();
        mobs = new ArrayList<Mob>();
        toDelete = new ArrayList<ImageActor>();
        pressedButtons = new HashSet<Integer>();
        player = new Player(Assets.get().imgs.get("imgs/car").getRegion(), 500, 300, 50);
        player.setImageRotation(90);
        stageCoords = new Vector2();
        //player.setScale(0.01f,0.01f);
        addActor(player);

        Timer.Task mobGenerator = new Timer.Task() {
            @Override
            public void run() {
                addMob(r.nextInt(Gdx.graphics.getWidth()),
                        r.nextInt(Gdx.graphics.getHeight()));
            }
        };
        Timer.schedule(mobGenerator, 5, 4, 10);
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        addMob(screenX, screenY);
        return true;
    }

    private void addMob(int screenX, int screenY) {
        stageCoords.set(screenX, screenY);
        screenToStageCoordinates(stageCoords);
        Mob mob = new Mob(
                "mob.png",
                stageCoords.x,
                stageCoords.y,
                50
        );
        mob.setTarget(player);
        mob.setImageRotation(90);
        mobs.add(mob);
        addActor(mob);
    }

    public void addBullet(Bullet bullet) {
        bullets.add(bullet);
        addActor(bullet);
    }

    public boolean keyDown(int keycode) {
        pressedButtons.add(keycode);
        return true;
    }

    public boolean keyUp(int keycode) {
        pressedButtons.remove(keycode);
        return true;
    }

    public Player getPlayer() {
        return player;
    }

    public void act(float delta) {
        float v = 100 * delta;
        if (player != null) {
            if (pressedButtons.contains(Input.Keys.UP)) player.move(v);
            if (pressedButtons.contains(Input.Keys.DOWN)) player.move(-v);
            if (pressedButtons.contains(Input.Keys.RIGHT)) player.rotateBy(-v);
            if (pressedButtons.contains(Input.Keys.LEFT)) player.rotateBy(v);
        }
        super.act(delta);
        toDelete.clear();
        for (ImageActor mob : mobs) {
            for (ImageActor bullet : bullets) {
                if (
                        Math.pow(mob.getX() - bullet.getX(), 2) +
                                Math.pow(mob.getY() - bullet.getY(), 2) <=
                                Math.pow(
                                        (
                                                Math.max(mob.getHeight(), mob.getWidth())
                                                        + Math.max(bullet.getHeight(), bullet.getWidth())
                                        ) / 2,
                                        2)
                        ) {
                    toDelete.add(mob);
                    toDelete.add(bullet);
                }
            }
        }
        for (int i = 0; i < toDelete.size(); i++) {
            toDelete.get(i).remove();
            mobs.remove(toDelete.get(i));
            bullets.remove(toDelete.get(i));
        }
    }

    public void killPlayer() {
        player.remove();
        player = null;
        GunCar.getInstance().setScreen("Menu");
    }
}