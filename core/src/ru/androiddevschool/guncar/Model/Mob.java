package ru.androiddevschool.guncar.Model;

import com.badlogic.gdx.scenes.scene2d.Actor;

import ru.androiddevschool.guncar.View.ImageActor;

/**
 * Created by 03k1203 on 04.05.2017.
 */
public class Mob extends ImageActor {
    private Actor target;
    public Mob(String fileName, float x, float y, float diam) {
        super(fileName, x, y, diam);
        setName("mob");
    }
    public void act(float delta) {
        super.act(delta);
        if (target != null) {
            float v = 50 * delta;
            lookAt(target.getX(), target.getY());
            move(v);
            if (distanceToTarget() <= 60) {
                ((Player) target).getLives().dec();
                if (((Player) target).getLives().get() == 0) ((World) getStage()).killPlayer();
                remove();
            }
        }
    }

    private float distanceToTarget() {
        return (float) Math.sqrt(Math.pow(getX() - target.getX(), 2)+Math.pow(getY()-target.getY(),2));
    }

    public void setTarget(Actor target) {
        this.target = target;
    }
}