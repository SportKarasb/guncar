package ru.androiddevschool.guncar.Model;

import ru.androiddevschool.guncar.Utils.Assets;
import ru.androiddevschool.guncar.View.ImageActor;

/**
 * Created by 03k1203 on 04.05.2017.
 */
public class Bullet extends ImageActor {
    private float velocity;
    public Bullet(float x, float y, float rotation){
        super(Assets.get().drawable("ui/bullet").getRegion(), x, y);
        velocity = 150;
        setRotation(rotation);
        setName("bullet");
    }
    public void act(float delta){
        move(velocity*delta);
    }
}
