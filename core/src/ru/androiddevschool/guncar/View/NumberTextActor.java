package ru.androiddevschool.guncar.View;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by 03k1203 on 04.05.2017.
 */
public class NumberTextActor extends Actor {
    private BitmapFont font;
    private int value;

    public NumberTextActor(int number, float x, float y, BitmapFont font) {
        this.font = font;
        this.value = number;
        setPosition(x, y);
    }

    public void inc(int value) {
        this.value += value;
    }

    public void dec(int value) {
        this.value -= value;
    }

    public void inc() {
        inc(1);
    }

    public void dec() {
        dec(1);
    }

    public int get() {
        return value;
    }

    public void set(int value) {
        this.value = value;
    }

    public void draw(Batch batch, float parentAlpha) {
        font.draw(batch, String.format("%d", value), getX(), getY());
    }
}