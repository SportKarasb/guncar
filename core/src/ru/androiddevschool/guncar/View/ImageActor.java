package ru.androiddevschool.guncar.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Random;

/**
 * Created by 03k1203 on 04.05.2017.
 */
public class ImageActor extends Actor {
    public static Random r = new Random();
    private TextureRegion img;
    protected float imageRotation;

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        imageRotation = 0;
        setPosition(x, y);
        setSize(width, height);
        setOrigin(getWidth() / 2, getHeight() / 2);
    }

    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    public ImageActor(TextureRegion img) {
        this(img, 0, 0);
    }

    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }

    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y);
    }

    public ImageActor(Texture img) {
        this(new TextureRegion(img));
    }

    public ImageActor(String fileName, float x, float y, float width, float height) {
        this(new Texture(fileName), x, y, width, height);
    }

    public ImageActor(String fileName, float x, float y) {
        this(new Texture(fileName), x, y);
    }

    public ImageActor(String fileName) {
        this(new Texture(fileName));
    }

    public ImageActor(String fileName, float x, float y, float diam) {
        this(fileName, x, y);
        if (getWidth() > getHeight()) {
            setHeight(diam * getHeight() / getWidth());
            setWidth(diam);
        } else {
            setWidth(diam * getWidth() / getHeight());
            setHeight(diam);
        }
        setOrigin(getWidth() / 2, getHeight() / 2);
    }

    public ImageActor(TextureRegion region, float x, float y, float diam) {
        this(region, x, y);
        if (getWidth() > getHeight()) {
            setHeight(diam * getHeight() / getWidth());
            setWidth(diam);
        } else {
            setWidth(diam * getWidth() / getHeight());
            setHeight(diam);
        }
        setOrigin(getWidth() / 2, getHeight() / 2);
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img,
                getX() - getOriginX(), getY() - getOriginY(),
                getOriginX(), getOriginY(),
                getWidth(), getHeight(),
                getScaleX(), getScaleY(),
                super.getRotation() - imageRotation
        );
    }

    public void act(float delta) {
        super.act(delta);
    }

    public float getRotation() {
        return (float) Math.toRadians(super.getRotation());
    }

    public void setRotation(float rotation) {
        super.setRotation((float) Math.toDegrees(rotation));
    }

    public void setImageRotation(float degrees) {
        this.imageRotation = degrees;
    }

    public void move(float len) {
        moveBy(
                len * (float) Math.cos(getRotation()),
                len * (float) Math.sin(getRotation())
        );
    }

    public void lookAt(float x, float y) {
        setRotation((float) Math.atan2(y - getY(), x - getX()));
    }
}