package ru.androiddevschool.guncar.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.guncar.GunCar;


/**
 * Created by ga_nesterchuk on 06.04.2017.
 */
public class ScreenTraveler extends ClickListener {
    private String name;
    public ScreenTraveler(String name){
        super();
        this.name = name;
    }
    public void clicked(InputEvent event, float x, float y){
        GunCar.get().setScreen(name);
    }
}
