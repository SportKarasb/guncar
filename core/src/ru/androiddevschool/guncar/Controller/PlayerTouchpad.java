package ru.androiddevschool.guncar.Controller;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;

import ru.androiddevschool.guncar.Model.Player;

/**
 * Created by 03k1203 on 18.05.2017.
 */

public class PlayerTouchpad extends Touchpad {
    private Player player;
    private Camera camera;

    public PlayerTouchpad(float deadzoneRadius, TouchpadStyle style, Player player) {
        super(deadzoneRadius, style);
        this.player = player;
        this.camera = null;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public void act(float delta) {
        super.act(delta);
        float alpha = (float) Math.atan2(getKnobPercentY(), getKnobPercentX());
        float mult = (float) Math.sqrt(Math.pow(getKnobPercentX(), 2) + Math.pow(getKnobPercentY(), 2));
        if (mult!=0) {
            player.setRotation(alpha);
            player.moveBy(
                    3 * mult * (float) Math.cos(alpha),
                    3 * mult * (float) Math.sin(alpha)
            );
        }
        if (camera != null) {
            camera.position.x = player.getX();
            camera.position.y = player.getY();
        }
    }
}