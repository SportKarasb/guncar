package ru.androiddevschool.guncar.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import ru.androiddevschool.guncar.Controller.ScreenTraveler;
import ru.androiddevschool.guncar.Utils.Assets;

/**
 * Created by ga_nesterchuk on 06.04.2017.
 */
public class Menu extends StdScreen {
    public Menu(SpriteBatch batch) {
        super(batch);
        initUi(ui);
    }

    protected void initUi(Stage stage){
        TextButton button;
        Table table = new Table();
        table.setFillParent(true);

        button = new TextButton("PLAY", (TextButton.TextButtonStyle) Assets.get().btnStyles.get("long-text"));
        button.addListener(new ScreenTraveler("Play"));
        table.add(button).padBottom(20).row();
        button = new TextButton("SETTINGS", (TextButton.TextButtonStyle) Assets.get().btnStyles.get("long-text"));
        button.addListener(new ScreenTraveler("Settings"));
        table.add(button).padBottom(20).row();
        button = new TextButton("HIGH SCORE", (TextButton.TextButtonStyle) Assets.get().btnStyles.get("long-text"));
        button.addListener(new ScreenTraveler("Rating"));
        table.add(button).padBottom(20).row();
        button = new TextButton("HELP", (TextButton.TextButtonStyle) Assets.get().btnStyles.get("long-text"));
        button.addListener(new ScreenTraveler("Help"));
        table.add(button);

        stage.addActor(table);
    }
}
