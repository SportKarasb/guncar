package ru.androiddevschool.guncar.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.guncar.Controller.PlayerTouchpad;
import ru.androiddevschool.guncar.Model.Bullet;
import ru.androiddevschool.guncar.Model.Player;
import ru.androiddevschool.guncar.Model.World;
import ru.androiddevschool.guncar.Utils.Assets;

/**
 * Created by 03k1203 on 04.05.2017.
 */
public class Play extends StdScreen {
    World world;
    Button shoot;
    Player player;
    PlayerTouchpad touchpad;

    public Play(SpriteBatch batch) {
        super(batch);
        world = new World(stage.getViewport(), stage.getBatch());
        stage = world;
        player = world.getPlayer();
        shoot = new Button(Assets.get().btnStyles.get("small"));
        shoot.addListener(
                new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        ((World)stage).addBullet(new Bullet(player.getX(), player.getY(), player.getRotation()));
                    }
                }
        );
        shoot.setPosition(Gdx.graphics.getWidth() - 10 - shoot.getWidth(), 10);
        ui.addActor(shoot);

        touchpad = new PlayerTouchpad(0, Assets.get().touchpadStyle, player);
        touchpad.setCamera(world.getCamera());
        touchpad.setPosition(100,100);
        ui.addActor(touchpad);

    }

    protected void postAct() {
        if (((World) stage).getPlayer() != null) {
            camera.position.x = ((World) stage).getPlayer().getX();
            camera.position.y = ((World) stage).getPlayer().getY();
        }
    }

    public void show(){
        super.show();
        Assets.get().music.get("bgm").play();
    }

    public void hide(){
        super.hide();
        Assets.get().music.get("bgm").stop();
    }
}