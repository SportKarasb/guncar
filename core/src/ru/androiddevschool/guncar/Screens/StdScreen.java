package ru.androiddevschool.guncar.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import ru.androiddevschool.guncar.Utils.Values;

/**
 * Created by ga_nesterchuk on 06.04.2017.
 */
class StdScreen implements Screen {
    protected OrthographicCamera camera;
    protected OrthographicCamera uiCamera;
    protected Stage stage;
    protected Stage ui;
    protected InputMultiplexer multiplexer;

    public StdScreen(SpriteBatch batch) {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false);
        stage = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, camera), batch);

        uiCamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        uiCamera.setToOrtho(false);
        ui = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, uiCamera), batch);
    }

    @Override
    public void show() {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(ui);
        //multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);
    }


    private void draw() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        ui.draw();
    }

    protected void postAct() {

    }

    private void act(float delta) {
        ui.act(delta);
        stage.act(delta);
    }

    @Override
    public void render(float delta) {
        act(delta);
        postAct();
        draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().setScreenSize(width, height);
        ui.getViewport().setScreenSize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
        ui.dispose();
    }
}
