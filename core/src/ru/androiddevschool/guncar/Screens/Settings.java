package ru.androiddevschool.guncar.Screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.guncar.Controller.ScreenTraveler;
import ru.androiddevschool.guncar.Utils.Assets;

/**
 * Created by 03k1203 on 04.05.2017.
 */
public class Settings extends StdScreen {

    public Settings(SpriteBatch batch) {
        super(batch);
        Table table = new Table();
        table.setFillParent(true);
        //table.setDebug(true);
        table.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Button button;
        button = new Button(Assets.get().btnStyles.get("small"));
        button.addListener(new ScreenTraveler("Menu"));
        table.add(button).align(Align.topRight).row();
        table.add(new Label("THIS IS A SETTINGS SCREEN", new Label.LabelStyle(new BitmapFont(), Color.WHITE))).row();
        ui.addActor(table);
    }
}