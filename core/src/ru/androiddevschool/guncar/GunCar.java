package ru.androiddevschool.guncar;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import ru.androiddevschool.guncar.Screens.*;

public class GunCar extends Game {
    public static GunCar instance = new GunCar();

    private GunCar() {
    }

    public static GunCar get() {
        return instance;
    }

    public static GunCar getInstance() {
        return instance;
    }

    @Override
    public void create() {
        batch = new SpriteBatch();
        screens = new HashMap<String, Screen>();
        screens.put("Menu", new Menu(batch));
        screens.put("Play", new Play(batch));
        screens.put("Settings", new Settings(batch));
        screens.put("Rating", new Rating(batch));
        screens.put("Help", new Help(batch));
        setScreen("Menu");
    }

    public void setScreen(String name) {
        if (screens.containsKey(name)) setScreen(screens.get(name));
    }

    private SpriteBatch batch;
    private HashMap<String, Screen> screens;

}
